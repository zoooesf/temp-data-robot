import Vue from 'vue'
import VueRouter from 'vue-router'

import Lobby from '../views/Lobby.vue'
import ExpoHall from '../views/ExpoHall.vue'
import HelpDesk from '../views/HelpDesk.vue'
import Leaderboard from '../views/Leaderboard.vue'
import Networking from '../views/Networking.vue'
import Sessions from '../views/Sessions.vue'
import SocialWall from '../views/SocialWall.vue'
import NetworkingSubPage from '../views/NetworkingSubPage.vue'

import Product from '../views/Product.vue'
import SalesResources from '../views/SalesResources.vue'
import Partners from '../views/Partners.vue'
import GTMResources from '../views/GTMResources.vue'
import Marketing from '../views/Marketing.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Lobby',
        component: Lobby
    },
    {
        path: '/ExpoHall',
        name: 'ExpoHall',
        component: ExpoHall
    },
    {
        path: '/HelpDesk',
        name: 'HelpDesk',
        component: HelpDesk
    },
    {
        path: '/Leaderboard',
        name: 'Leaderboard',
        component: Leaderboard
    },
    {
        path: '/Networking',
        name: 'Networking',
        component: Networking
    },
    {
        path: '/NetworkSubPage/:frameUrl',
        name: 'NetworkSubPage',
        component: NetworkingSubPage
    },
    {
        path: '/Sessions',
        name: 'Sessions',
        component: Sessions
    },
    {
        path: '/SocialWall',
        name: 'SocialWall',
        component: SocialWall
    },
    {
        path: '/Product',
        name: 'Product',
        component:Product 
    },
    {
        path: '/SalesResources',
        name: 'SalesResources',
        component:SalesResources 
    },
    {
        path: '/Partners',
        name: 'Partners',
        component:Partners 
    },
    {
        path: '/GTMResources',
        name: 'GTMResources',
        component:GTMResources 
    },
    {
        path: '/Marketing',
        name: 'Marketing',
        component:Marketing 
    }
]

const router = new VueRouter({
    hashbang: false,
    history: true,
    linkActiveClass: 'active',
    transitionOnLoad: true,
    root: '/',
    mode: 'history',
    routes
})

export default router