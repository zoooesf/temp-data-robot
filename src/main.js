import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { VBHoverPlugin } from 'bootstrap-vue'
import Zendesk from '@dansmaculotte/vue-zendesk'
import axios from 'axios'
import VueAxios from 'vue-axios'
import SuiVue from 'semantic-ui-vue';
import VueCookies from 'vue-cookies'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'semantic-ui-css/semantic.min.css';
// import './assets/css/myCssNonsense.css'
// import '../semantic/dist/semantic.min.css';

Vue.use(SuiVue);
Vue.use(VueAxios, axios)
Vue.use(VBHoverPlugin)
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.productionTip = false
Vue.use(VueCookies)

Vue.use(Zendesk, {
  key: '768ece47-9291-4da8-a2af-0b9ae653093e',
  disabled: false,
  hideOnLoad: false,
  settings: {
    webWidget: {
      color: {
        theme: '#FF5600'
      }
    }
  }})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
